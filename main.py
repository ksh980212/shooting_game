import pygame
import random
from time import sleep

pad_width=400
pad_height =800
size=[pad_width,pad_height]
#background size

fighter_width= 36
fighter_height=20
#fighter size

enemy_width=26
enemy_height=20
#enemy_size

BLACK =(0,0,0)
WHITE = (255,255,255)
BLUE= (0, 0, 255)
GREEN= (0,255,0)
RED= (255,0,0)
#define the colors we will use in RGB format

def drawScore(count):
	global gamepad
	font = pygame.font.SysFont(None, 20)
	text= font.render('Enemy Kills: '+str(count), True, (255,255,255))
	gamepad.blit(text,(0,0))

def drawPassed(count):
	global gamepad
	font =pygame.font.SysFont(None,20)
	text = font.render('Enemy Passed: '+str(count), True, RED)
	gamepad.blit(text, (280,0))

def drawSpeed(speed):
	global gamepad,enemy_speed
	font=pygame.font.SysFont(None,20)
	text= font.render('Enemy Speed: '+str(enemy_speed), True, WHITE)
	gamepad.blit(text, (250,760))

def dispMessage(text):
	global gamepad
	textfont= pygame.font.Font('freesansbold.ttf', 40)
	text=textfont.render(text, True, RED)
	textpos= text.get_rect()
	textpos.center=(pad_width/2, pad_height/2)
	gamepad.blit(text, textpos)
	pygame.display.update()
	sleep(5)
	runGame()

def crash():
	global gamepad, shotcount
	dispMessage('END_Score: %d'%shotcount)

def gameover():
	global gamepad, shotcount
	dispMessage('END_Score: %d'%shotcount)

def drawObject(obj, x, y):
	global gamepad
	gamepad.blit(obj,(x,y))

def initGame():
	global gamepad, fighter,enemy,clock, bullet,background_img

	pygame.init()
	gamepad= pygame.display.set_mode(size)
	pygame.display.set_caption("My First Shooting Game!!")
	background_img = pygame.image.load('background.png')
	fighter= pygame.image.load('fighter.png')
	enemy= pygame.image.load('enemy.png')
	bullet= pygame.image.load('bullet.png')


	clock= pygame.time.Clock()

def runGame():
	global gamepad, fighter,enemy, clock, bullet,background_img, shotcount,enemy_speed

	isShoted =False
	shotcount=0
	enemy_passed= 0

	bullet_xy=[]

	#fighter position(x,y)
	x= pad_width* 0.45
	y= pad_height*0.9
	x_change= 0

	#enemy position
	enemy_x= random.randrange(0,pad_width- enemy_width)
	enemy_y= 0
	enemy_speed=3

	ongame= False
	while not ongame:
		for event in pygame.event.get():
			if event.type==pygame.QUIT: #마우스로 창을 닫음
				ongame=True
			if event.type== pygame.KEYDOWN: 
				if event.key == pygame.K_LEFT: #키보드 왼쪽
					x_change-=8
				elif event.key==pygame.K_RIGHT: #키보드 오른쪽 
					x_change+=8
				elif event.key==pygame.K_SPACE:
					if len(bullet_xy)< 3:
						bullet_x= x+fighter_width/2
						bullet_y= y-fighter_height
						bullet_xy.append([bullet_x,bullet_y])
			if event.type==pygame.KEYUP:
				if event.key == pygame.K_LEFT or event.key==pygame.K_RIGHT:
					x_change=0


		# gamepad.fill(BLACK)
		gamepad.blit(background_img, (0,0))
		drawSpeed(enemy_speed)

		x+=x_change
		if x<0:
			x=0
		elif x>pad_width-fighter_width:
			x= pad_width-fighter_width

		if y<enemy_y+enemy_height:
			if(enemy_x> x and enemy_x < x+fighter_width)or\
			(enemy_x+ enemy_width> x and enemy_x+ enemy_width<x+fighter_width):
				crash()
		
		drawObject(fighter,x,y)

		if len(bullet_xy)!= 0:
			for i, bxy in enumerate(bullet_xy):
				bxy[1] -=10
				bullet_xy[i][1]= bxy[1]

				if bxy[1]<enemy_y:
						if bxy[0]>= enemy_x-enemy_width and bxy[0] <=enemy_x+enemy_width:
							bullet_xy.remove(bxy)
							isShoted=True
							shotcount+=1
				if bxy[1]<=0:
					try:
						bullet_xy.remove(bxy)
					except:
						pass

		if len(bullet_xy)!=0:
			for bx, by in bullet_xy:
				drawObject(bullet, bx, by)
		
		drawScore(shotcount)
		#enemy move
		enemy_y+=enemy_speed

		if enemy_y>pad_height:
			enemy_y=0
			enemy_x= random.randrange(0, pad_width-enemy_width)
			enemy_passed+=1

		if enemy_passed==3:
			gameover()

		drawPassed(enemy_passed)

		if isShoted:
			# enemy_speed+=1
			enemy_speed+=random.randrange(-5,5)
			if enemy_speed>10:
				enemy_speed=10
			if enemy_speed<3:
				enemy_speed=3

			enemy_x= random.randrange(0, pad_width-enemy_width)
			enemy_y=0
			isShoted=False

		drawObject(enemy, enemy_x, enemy_y)

		pygame.display.update()
		clock.tick(60)
	pygame.quit()				

initGame()
runGame()
